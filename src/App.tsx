import { Col, Divider, Input, InputNumber, Popover, Row } from "antd";
import React, { useEffect, useState } from "react";
import "./App.scss";
import { getAllIterations, Iteration } from "./utils";

const sheetDetails = ({
  lengthUsed,
  lengthUnused,
  optimalPieces,
}: Iteration) => (
  <table>
    <tbody>
      <tr>
        <th>Used material</th>
        <td>{lengthUsed} mm</td>
      </tr>
      <tr>
        <th>Surplus material</th>
        <td>{lengthUnused} mm</td>
      </tr>
      <tr>
        <th>Count of pieces</th>
        <td>{optimalPieces.length}</td>
      </tr>
      <tr>
        <th>Used pieces</th>
        <td>
          {optimalPieces.map((piece, index) => (
            <div key={`used-piece-${index}`}>{piece} mm</div>
          ))}
        </td>
      </tr>
    </tbody>
  </table>
);

const pieceDetails = (piece: number) => (
  <table>
    <tbody>
      <tr>
        <th>Length</th>
        <td>{piece} mm</td>
      </tr>
    </tbody>
  </table>
);

const setItem = (
  key: string,
  value: number | number[],
  callback?: (value: number | number[]) => void
) => {
  localStorage.setItem(key, JSON.stringify(value));
  if (callback) {
    callback(value);
  }
};

const getItem = (key: string, initialValue: number | number[]) =>
  JSON.parse(localStorage.getItem(key) || JSON.stringify(initialValue));

const App = () => {
  const [length, setLength] = useState(getItem("length", 1000));
  const [pieces, setPieces] = useState(
    getItem(
      "pieces",
      [500, 400, 300, 200, 100, 300, 100, 95, 1400, 2000, 1000, 300, 503]
    )
  );
  const [ratio, setRatio] = useState(window.visualViewport.width / length);

  useEffect(() => {
    const onResize = () => {
      setRatio(window.visualViewport.width / length);
    };

    window.addEventListener("resize", onResize);

    return () => {
      window.removeEventListener("resize", onResize);
    };
  }, [length]);

  const iterations = getAllIterations(
    length,
    pieces.filter(Boolean).filter((a: number) => a <= length)
  );
  const invalidPieces: number[] = pieces
    .filter(Boolean)
    .filter((a: number) => a > length);

  return (
    <div className="App">
      <div className="Inputs">
        <div className="sheet">
          Sheet length:{" "}
          <InputNumber
            placeholder="Piece length, e.g.: 1000"
            addonAfter="mm"
            defaultValue={length}
            onChange={(value) => setItem("length", value, setLength)}
          />
        </div>
        <div className="pieces">
          Pieces:{" "}
          <Input
            placeholder="Comma separated, e.g.: 100, 200, 500, 300, 1400, 900, 400"
            onChange={({ target: { value } }) => {
              setItem("pieces", value.split(",").map(parseFloat), setPieces);
            }}
            defaultValue={pieces.join(", ")}
          />
        </div>
      </div>

      {iterations.map((iteration) => (
        <div key={`iteration-${iteration.iteration}`}>
          <Popover
            arrowPointAtCenter
            title={`Sheet ${iteration.iteration}`}
            content={sheetDetails(iteration)}
          >
            <Divider plain>Sheet {iteration.iteration}</Divider>
          </Popover>
          <Row>
            {iteration.optimalPieces.map((piece, index) =>
              !!piece && piece > 0 ? (
                <Popover
                  arrowPointAtCenter
                  placement={
                    iteration.optimalPieces.length === 1 ||
                    (index > 0 && index < iteration.optimalPieces.length - 1)
                      ? "top"
                      : index === iteration.optimalPieces.length - 1
                      ? "topRight"
                      : "topLeft"
                  }
                  key={`iteration-${iteration.iteration}-piece-${piece}-index-${index}`}
                  title={`Sheet ${iteration.iteration}, Piece ${index + 1}`}
                  content={pieceDetails(piece)}
                >
                  <Col flex={`1 1 ${piece * ratio}px`}>{piece} mm</Col>
                </Popover>
              ) : (
                ""
              )
            )}
            {iteration.lengthUnused ? (
              <Popover
                arrowPointAtCenter
                placement="topRight"
                title={`Sheet ${iteration.iteration} - Surplus`}
                content={pieceDetails(iteration.lengthUnused)}
              >
                <Col
                  className="unused"
                  flex={`1 1 ${iteration.lengthUnused * ratio}px`}
                >
                  {iteration.lengthUnused} mm
                </Col>
              </Popover>
            ) : (
              ""
            )}
          </Row>
        </div>
      ))}

      {invalidPieces.length ? (
        <>
          <Divider plain>Invalid pieces ({invalidPieces.length})</Divider>
          {invalidPieces.map((piece, index) => (
            <div key={`invalid-piece-${piece}-${index}`}>{piece} mm</div>
          ))}
        </>
      ) : (
        ""
      )}
    </div>
  );
};

export default App;
