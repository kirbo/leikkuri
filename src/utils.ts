export interface OptimalPieces {
  lengthUsed: number;
  lengthUnused: number;
  optimalPieces: number[];
  unusedPieces: number[];
}

export type Iteration = {
  iteration: number;
} & OptimalPieces;

export const findOptimalPieces = (
  length: number,
  pieces: number[]
): OptimalPieces => {
  const optimalPieces: number[] = [];
  const unusedPieces: number[] = [];
  const lengthUsed = pieces.reduce((previous, current) => {
    if (previous + current <= length) {
      optimalPieces.push(current);
      return previous + current;
    }

    unusedPieces.push(current);
    return previous;
  }, 0);

  return {
    lengthUsed,
    lengthUnused: length - lengthUsed,
    optimalPieces,
    unusedPieces,
  };
};

export const getOptimalPieces = (
  length: number,
  pieces: number[]
): OptimalPieces => {
  const ascending = [...pieces];
  ascending.sort((a, b) => a - b);
  const descending = [...ascending].reverse();

  const order1: number[] = [];
  const order2: number[] = [];

  let asc = -1;
  let desc = pieces.length;
  pieces.forEach((_, index) => {
    if (index % 2) {
      asc++;
      order1.push(ascending[asc]);
      order2.push(descending[asc]);
    } else {
      desc--;
      order1.push(ascending[desc]);
      order2.push(descending[desc]);
    }
  });

  // console.log("order1", order1);
  // console.log("order2", order2);

  const iterations: OptimalPieces[] = [
    findOptimalPieces(length, ascending),
    findOptimalPieces(length, descending),
    findOptimalPieces(length, order1),
    findOptimalPieces(length, order2),
  ];

  const optimal: OptimalPieces = iterations.reduce(
    (previous: OptimalPieces, current: OptimalPieces) => {
      return previous.lengthUsed < current.lengthUsed ? current : previous;
    },
    iterations[0]
  );

  return optimal;
};

export const getAllIterations = (
  length: number,
  pieces: number[],
  iteration: number = 1
): Iteration[] => {
  const iterations: Iteration[] = [
    { iteration, ...getOptimalPieces(length, pieces) },
  ];
  if (iterations[0].unusedPieces.length) {
    iterations.push(
      ...getAllIterations(length, iterations[0].unusedPieces, iteration + 1)
    );
  }

  return iterations;
};
